<!DOCTYPE html>
<html lang="en">
<?php

?>
    <head> 
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

		<!-- Website CSS style -->
		<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">

		<!-- Website Font style -->
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
		
		<!-- Google Fonts -->
		<link href='https://fonts.googleapis.com/css?family=Passion+One' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>
		
		<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
		
		<script src="js/jquery/dist/jquery.validate.js"></script>
		
		 

		<title>Admin</title>
	</head>
	<body>
		
		<div class="container">
			<div class="row main">
				<div class="panel-heading">
	               <div class="panel-title text-center">
	               		<h1 class="title">Login</h1>
	               		<hr />
	               	</div>
	            </div> 
				<div class="main-login main-center">
					
					
					<form class="form-horizontal" method="POST" id= "submit" action="" >
						
						<div class="form-group">
							<label for="username" class="cols-sm-2 control-label">Username</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="username" id="username"  placeholder="Enter your Username" required/>
									
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="password" class="cols-sm-2 control-label">Password</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
									<input type="password" class="form-control" name="password" id="password"  placeholder="Enter your Password" required/>
									
								</div>
							</div>
						</div>

						

						<div class="form-group ">
							<input type="submit" name ="submit" class="btn btn-primary btn-lg btn-block login-button" value = "Submit">Register</input>
							
						</div>	
					</form>
				</div>
			</div>
		</div>
	</body>
	
	
</html>
<script type="text/javascript"> 
			
			
			$().ready(function() {
		// validate the comment form when it is submitted
		//$("#commentForm").validate();

		// validate signup form on keyup and submit
		$("#submit").validate({
			rules: {
				
				username: {
					required: true,
					minlength: 2
				},
				password: {
					required: true,
					minlength: 5
				}
				
			},
			messages: {
				
				username: {
					required: "Please enter a username",
					minlength: "Your username must consist of at least 2 characters"
				},
				password: {
					required: "Please provide a password",
					minlength: "Your password must be at least 5 characters long"
				},
				
				
				
			}
		});
	});
		</script>
<style>
.error{
color: red;
}

</style>

