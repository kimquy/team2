-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 02, 2017 at 12:58 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `login`
--

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(60) NOT NULL,
  `confirmpassword` varchar(60) NOT NULL,
  `avatar` varchar(10000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `name`, `email`, `username`, `password`, `confirmpassword`, `avatar`) VALUES
(2, 'Dang Thu Lan', 'danglan@gmail.com', 'danglan', '$2y$10$v0UGCGEkWKWpZi4nItJt4uFht1XVUY9snTjCa87Cub5QcEwz5G5La', '123456', ''),
(3, 'Dang Kim Quy', 'kimquydkq@gmail.com', 'kimquy', '$2y$10$V35j9JmIyKrLwFv9.Wvofupaea0UDc55cYXNoecAjq38rqvG5rwWa', '123456', ''),
(8, 'Nguyen Ngoc Tuong', 'tuong@gmail.com', 'ngoctuong', '$2y$10$c6QByFGa/1AkYVma/S.iteyNBpD3TE0grV7wWr.iVmP8OLCn9LQ.u', '123456', ''),
(12, 'Nguyen Thi Thu', 'thu@gmail.com', 'thunguyen', '$2y$10$b5lRFg0Y6leuqnow/3DIb.P7fKgKVTeoQSaMA/EqTbq5om9qK.p42', '123456', ''),
(15, 'Ngo Phuong Thao', 'thao@gmail.com', 'phuongthao', '$2y$10$yf1.jd6trgjWxIIv5GUCHejfW2r641XLSngfmsC.tkV6XHg4RyJ4i', '123456', ''),
(19, 'Dang Thanh Nam', 'thanhnam@gmail.com', 'thanhnam', '$2y$10$oLsqvAKqZ0zWFdszVnNg..w1bT2OA8vGUb54n36uU/VB1KWMC6mPO', '123456', ''),
(21, 'Nguyen Linh Chi ', 'linhchi@gmail.com', 'linhchi', '$2y$10$qkFKjfeXVou0imnY7B3PEulXQwrFkyK7OtoC5buQdH72oVAPHEWUW', '123456', ''),
(22, 'Nguyen Thu Thao', 'thuthao@gmail.com', 'thuthao', '$2y$10$mgU8Y.FZWKbeNRHHnapDMe.ElucMzFZeP9xCQOX2AVEwv9rKVcqY6', '123456', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
